{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Physics 256\n",
    "## Lecture 08 - I/O & Numpy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"http://www.numpy.org/_static/numpy_logo.png\" width=300px>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import style\n",
    "style._set_css_style('../include/bootstrap.css')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Last Time\n",
    "\n",
    "### [Notebook Link: 07_InputOutput.ipynb](./06_InputOutput.ipynb)\n",
    "\n",
    "- wrote a module to compute $\\pi$ in various ways\n",
    "- saw Monte Carlo for the first time!\n",
    "- working with the filesystem\n",
    "- file input/output\n",
    "\n",
    "## Today\n",
    "- working with numerical data\n",
    "- introduction to Numpy\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"span alert alert-success\">\n",
    "<h2> Team programming challenge </h2>\n",
    "<h3> Average energy for the simple harmonic oscillator at finite temperature</h3>\n",
    "</div>\n",
    "\n",
    "I have uploaded a data file on BlackBoard in the Week 03 Course Materials called `sho_energy.dat`.  The line contains column headings with units in kelvin.\n",
    "\n",
    "The next set of lines contain quantum Monte Carlo measurements for the kinetic and potential energy of the simple harmonic osscilator at $T = 0.5~\\mathrm{K}$ where $\\hbar \\omega/k_{\\mathrm{B}} = 1$.  The exact answer is known to be:\n",
    "\\begin{equation}\n",
    "E(T) = \\frac{\\hbar \\omega}{2} \\coth \\frac{\\hbar \\omega}{2 k_{\\mathrm{B}} T}.\n",
    "\\end{equation}\n",
    "\n",
    "Download the data file to your computer and write a program that loads the file from disk and computes the average total energy of all lines.  If you have extra time, compare with the exact result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!cat ../data/sho_energy.dat"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%load \"solutions/sho_average.py\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## NumPy\n",
    "\n",
    "NumPy is the fundamental package needed for scientific computing with Python. It contains among other things:\n",
    "- a powerful N-dimensional array object \n",
    "- sophisticated and optimized array (broadcasting) functions\n",
    "- tools for integrating C/C++ and Fortran code \n",
    "- useful linear algebra, Fourier transform, and random number capabilities\n",
    "\n",
    "We can import all this functionality using the `import` command.  It is customary to use the shortform `np`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Using Numpy!\n",
    "data = np.genfromtxt('../data/sho_energy.dat',names=True, comments='#')\n",
    "print(f'Total Energy = {np.average(data[\"Kinetic\"]+data[\"Potential\"]):5.3f}K')\n",
    "print(f'Exact Energy = {0.5/np.tanh(1.0):5.3f}K')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "for name in data.dtype.names:\n",
    "    print(name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NumPy contains array-optimized versions of all the standard library math functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(np.sqrt(1.234))\n",
    "\n",
    "# can deal with complex numbers\n",
    "print(np.conjugate(1+3j))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most important part of NumPy for us is the array datatype.  We can create arrays using:\n",
    "- python lists\n",
    "- special functions `zeros`, `ones`, `arange`, `linspace`, etc.\n",
    "- directy from files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creation from lists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a = np.array([0,1,2,3,4])\n",
    "print(a)\n",
    "b = np.array([i for i in range(100)])\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# It will try to dynamically guess\n",
    "# the type unless we specify with ‘dtype’\n",
    "a = np.array([0,1,2],dtype=float)\n",
    "a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"span alert alert-danger\">\n",
    "Numpy arrays are **statically typed**.  This allows them to be extremely fast with highly optimized array operations.  However, we cannot use them as heterogenous object collections.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a[0] = 'hello'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# use dtype to see the type of an array\n",
    "a.dtype"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using array creation functions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# `arange` is like `range` except it can produce lists of floats\n",
    "a = np.arange(0,10,0.5)\n",
    "a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.sqrt(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# zeros and ones work as expected\n",
    "a0 = np.zeros(10)\n",
    "a1 = np.ones(10)\n",
    "\n",
    "a2 = np.zeros_like(a1)\n",
    "\n",
    "print(a0)\n",
    "print(a1)\n",
    "print(a2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# linspace allows us to divide a region in to N linear pieces\n",
    "a = np.linspace(0,1,10)\n",
    "a"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# `logspace` can do the same for exponential functions\n",
    "l = np.logspace(0, 10, 10, base=np.e)\n",
    "l"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"span alert alert-danger\">\n",
    "Note that when using linspace, the endpoints are included!!!\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## We can also make arrays filled with random numbers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "r = np.random.random(100)\n",
    "r"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The numpy routines are **much** faster than the built in python methods"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# making an array of squares\n",
    "%timeit [i**2 for i in range(1000)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%timeit np.arange(1000)**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Indexing\n",
    "\n",
    "Works exactly the same as for python lists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a = np.arange(10)**3\n",
    "print(a)\n",
    "print(a[1])\n",
    "print(a[:3])\n",
    "print(a[-1:-3:-1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting help\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "help(np.diagflat)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Array operations\n",
    "\n",
    "<div class=\"span alert alert-danger\">\n",
    "These work on an element-by-element basis, and operate mathematically, unlike python lists\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a1 = np.arange(0.0,2.0*np.pi,np.pi/6)\n",
    "a2 = np.ones_like(a1)\n",
    "print(a1)\n",
    "print(a2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a1 + a2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "2*(a1+a2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "(a1+a2)**3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.log(a1+a2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multi-dimensional arrays\n",
    "\n",
    "We can create them with nested lists"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "M = np.array([[1,2,3,4],[5,6,7,8]])\n",
    "M"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# `shape` gives us information about an array\n",
    "M.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# while `size` gives us the total number of elements\n",
    "M.size"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# index using a simple list of indices\n",
    "M[1,3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "M.flatten().reshape(2,4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can pass our array creation functions the size of a desired multi-dimensional array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "M1 = np.ones([3,3])\n",
    "M1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "Id = np.identity(3)\n",
    "print(Id)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "np.diag(M1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# can go beyond matrices\n",
    "M0 = np.zeros([3,3,3])\n",
    "M0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# there are some special matrix creaters\n",
    "II = np.identity(4, dtype=int)\n",
    "II"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"span alert alert-success\">\n",
    "<h2> Team Programming Challenge </h2>\n",
    "<ol>\n",
    "<li>Find indices of non-zero elements from [1,2,0,0,4,0].</li>\n",
    "<li>Create an array of uniformly distributed random numbers between 1 and 3 and explore how the average depends on the length of the array.\n",
    "<li> Show that a 10x10 identity matrix is idempotent. </li>\n",
    "<li> Explore the meshgrid method and use it to evaluate the matrix $Z = X^2 + Y^2\n",
    "$ on a 10x10 grid between -1,1.\n",
    "</ol>\n",
    "</div>\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
